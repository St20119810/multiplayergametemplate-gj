#ifndef SOCKET_ADDRESS_H_
#define SOCKET_ADDRESS_H_

#include "NetworkingCommon.h"

#include <string>
#include <memory>
#include <vector>

using std::shared_ptr;
using std::vector;
using std::string;

//For mem copy
#include <cstring>

/** 
Socket Address handles the creation and setting up of our codebases sockets.

For this codebase only UDP is used and all connections are assumed to be IPv4.
*/

class SocketAddress
{
public:
	///Creation of the socket.
	SocketAddress( uint32_t inAddress, uint16_t inPort )
	{
		///AF_INET refers to the family we are working in and that is IPv4.
		GetAsSockAddrIn()->sin_family = AF_INET;
		///htonl means Host TO Network Long - for changing bits from little endian to big endian.
		GetIP4Ref() = htonl( inAddress );
		///htons means Host TO Network Short - for changing bits from big endian to little endian.
		GetAsSockAddrIn()->sin_port = htons( inPort );
	}
	
	SocketAddress( const sockaddr& inSockAddr )
	{
		memcpy( &mSockAddr, &inSockAddr, sizeof( sockaddr ) );
	}

	SocketAddress()
	{
		GetAsSockAddrIn()->sin_family = AF_INET;
		///Here we're allowing the compiler to pick any available address from the network.
		GetIP4Ref() = INADDR_ANY;
		///We have specified port 0.
		GetAsSockAddrIn()->sin_port = 0;
	}

	bool operator==( const SocketAddress& inOther ) const
	{
		return ( mSockAddr.sa_family == AF_INET &&
				 GetAsSockAddrIn()->sin_port == inOther.GetAsSockAddrIn()->sin_port ) &&
				 ( GetIP4Ref() == inOther.GetIP4Ref() );
	}

	size_t GetHash() const
	{
		return ( GetIP4Ref() ) |
			( ( static_cast< uint32_t >( GetAsSockAddrIn()->sin_port ) ) << 13 ) |
			mSockAddr.sa_family;
	}


	uint32_t				GetSize()			const	{ return sizeof( sockaddr ); }

	string					ToString()			const;


	//For testing
	///When testing what IP address we have we can use this get.
  uint32_t getIP4() {return GetIP4Ref();}
  ///When testing what Port we have assigned we can use this get.
	uint16_t getPort() {return ntohs(GetAsSockAddrIn()->sin_port);}
	///When testing what Address Family we have set we can use this get.
	uint16_t getFamily() {return GetAsSockAddrIn()->sin_family;}

private:
	friend class UDPSocket;
	friend class TCPSocket;

	sockaddr mSockAddr;
#if _WIN32
	uint32_t&				GetIP4Ref()					{ return *reinterpret_cast< uint32_t* >( &GetAsSockAddrIn()->sin_addr.S_un.S_addr ); }
	const uint32_t&			GetIP4Ref()			const	{ return *reinterpret_cast< const uint32_t* >( &GetAsSockAddrIn()->sin_addr.S_un.S_addr ); }
#else
	uint32_t&				GetIP4Ref()					{ return GetAsSockAddrIn()->sin_addr.s_addr; }
	const uint32_t&			GetIP4Ref()			const	{ return GetAsSockAddrIn()->sin_addr.s_addr; }
#endif

	sockaddr_in*			GetAsSockAddrIn()			{ return reinterpret_cast< sockaddr_in* >( &mSockAddr ); }
	const	sockaddr_in*	GetAsSockAddrIn()	const	{ return reinterpret_cast< const sockaddr_in* >( &mSockAddr ); }

};

typedef shared_ptr< SocketAddress > SocketAddressPtr;

namespace std
{
	template<> struct hash< SocketAddress >
	{
		size_t operator()( const SocketAddress& inAddress ) const
		{
			return inAddress.GetHash();
		}
	};
}

#endif

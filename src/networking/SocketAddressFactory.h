#ifndef SOCKET_ADDRESS_FACTORY_H_
#define SOCKET_ADDRESS_FACTORY_H_

#include "SocketAddress.h"
/** 
This class is used to identify the format of the address structure for our sockets.
*/
class SocketAddressFactory
{
public:
	///We are using IPv4 socket address family (AF_INET).
	static SocketAddressPtr CreateIPv4FromString( const string& inString );
};

#endif

#include "PlayerServer.h"
#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"
#include "Bullet.h"
#include "GameObjectRegistry.h"
#include <iostream>
#include "Wall.h"


PlayerServer::PlayerServer() :
	mPlayerControlType( ESCT_Human ),
	mTimeOfNextShot( 0.f ),
	mTimeBetweenShots( 2.0f )
{}

void PlayerServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}

void PlayerServer::Update()
{
	Player::Update();


	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();
	float oldRotation = GetRotation();

	ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy( GetPlayerId() );
	if( client )
	{
		MoveList& moveList = client->GetUnprocessedMoveList();
		for( const Move& unprocessedMove : moveList )
		{
			const InputState& currentState = unprocessedMove.GetInputState();
			float deltaTime = unprocessedMove.GetDeltaTime();
			ProcessInput( deltaTime, currentState );
			SimulateMovement( deltaTime );
		}

		moveList.Clear();
	}

	HandleShooting();

	if( !Maths::Is2DVectorEqual( oldLocation, GetLocation() ) ||
		!Maths::Is2DVectorEqual( oldVelocity, GetVelocity() ) ||
		oldRotation != GetRotation() )
	{
		NetworkManagerServer::sInstance->SetStateDirty( GetNetworkId(), EPRS_Pose );
	}
}

void PlayerServer::HandleShooting()
{
	float time = Timing::sInstance.GetFrameStartTime();
	if( mIsShooting && Timing::sInstance.GetFrameStartTime() > mTimeOfNextShot )
	{
		//not exact, but okay
		mTimeOfNextShot = time + mTimeBetweenShots;

		//fire!
		std::shared_ptr<Bullet> bullet = std::static_pointer_cast<Bullet>( GameObjectRegistry::sInstance->CreateGameObject( 'BULL' ) );
		//std::shared_ptr<Wall> wall = std::static_pointer_cast<Wall>(GameObjectRegistry::sInstance->CreateGameObject('WALL'));
		bullet->Init(this);

		bullet->SetLocation(GetLocation() + (0.4f * GetForwardVector()));
		bullet->SetRotation(GetRotation());
		//wall->SetLocation(GetLocation() + (0.4f * GetForwardVector()));
		//wall->SetRotation(GetRotation());
	}
}

void PlayerServer::TakeDamage( int inDamagingPlayerId )
{
	mHealth--;
	if( mHealth <= 0.f )
	{
		//score one for damaging player...
		//ScoreBoardManager::sInstance->IncScore( inDamagingPlayerId, 1 );

		//and you want to die
		SetDoesWantToDie( true );
		
		//tell the client proxy to make you a new player
		ClientProxyPtr clientProxy = NetworkManagerServer::sInstance->GetClientProxy( GetPlayerId() );
		if( clientProxy )
		{
			//clientProxy->HandlePlayerDied();
			for (auto client : NetworkManagerServer::allClients)
			{
				client->player->SetDoesWantToDie(true);
				client->HandlePlayerDied();
			
			}
		}
	}

	//tell the world our health dropped
	NetworkManagerServer::sInstance->SetStateDirty( GetNetworkId(), EPRS_Health );
}

#include "ClientProxy.h"
#include "Timing.h"
#include "Engine.h"
#include "Server.h"


namespace
{
	const float kRespawnDelay = 3.f;
}

ClientProxy::ClientProxy( const SocketAddress& inSocketAddress, const string& inName, int inPlayerId ) :
mSocketAddress( inSocketAddress ),
mName( inName ),
mPlayerId( inPlayerId ),
mIsLastMoveTimestampDirty( false ),
mTimeToRespawn( 0.f )
{
	UpdateLastPacketTime();
}


void ClientProxy::UpdateLastPacketTime()
{
	mLastPacketFromClientTime = Timing::sInstance.GetTimef();
}

void	ClientProxy::HandlePlayerDied()
{
	mTimeToRespawn = Timing::sInstance.GetFrameStartTime() + kRespawnDelay;
}

void	ClientProxy::RespawnPlayerIfNecessary()
{
	if( mTimeToRespawn != 0.f && Timing::sInstance.GetFrameStartTime() > mTimeToRespawn )
	{
		//if (mPlayerId == 1) {
		//	//respawn player 1
		//	static_cast<Server*> (Engine::sInstance.get())->SpawnPlayer(1);
		//	mTimeToRespawn = 0.0f;
		//	//then respawn player 2
		//	static_cast<Server*> (Engine::sInstance.get())->SpawnPlayer(2);
		//	mTimeToRespawn = 0.0f;
		//}
		//else if (mPlayerId == 2) {
		//	//respawn player 2
		//	static_cast<Server*> (Engine::sInstance.get())->SpawnPlayer(2);
		//	mTimeToRespawn = 0.0f;
		//	//then respawn player 1
		//	static_cast<Server*> (Engine::sInstance.get())->SpawnPlayer(1);
		//	mTimeToRespawn = 0.0f;
		//}
		//else {
		//	//only supposed to be 2 players so don't bother respawning a third player
		//}
		player = static_cast< Server* > ( Engine::sInstance.get() )->SpawnPlayer( mPlayerId );

		mTimeToRespawn = 0.0f;
		
	}
}

#include "Wall.h"
#include "NetworkManagerServer.h"
/** 
The server side of the Wall Object.

This class is straight forward, it sets up the wall object with the NetworkManagerServer.
*/
class WallServer : public Wall 
{
public:
	///This function registers the Wall with the NetworkManagerServer.
	static GameObjectPtr StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new WallServer()); }
	///If the Wall were to die, what would happen?
	void HandleDying() override;
	//If the player were to collide with the Wall, what should happen?
	/*virtual bool HandleCollisionWithPlayer(Player* inPlayer) override;*/
protected:
	WallServer();
};

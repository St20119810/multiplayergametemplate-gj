
#include "Server.h"
#include "GameObjectRegistry.h"
#include "StringUtils.h"
#include "Colors.h"
#include "PlayerServer.h"
#include "BulletServer.h"
#include "WallServer.h"
#include "Maths.h"
#include "GetRandomVector.h"
#include <Vector3.h>
#include <random>
//sneaky

bool Server::StaticInit()
{
	sInstance.reset( new Server() );

	return true;
}

Server::Server()
{

	GameObjectRegistry::sInstance->RegisterCreationFunction( 'PLYR', PlayerServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'BULL', BulletServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'WALL', WallServer::StaticCreate );

	InitNetworkManager();

	// Setup latency
	float latency = 0.0f;
	string latencyString = StringUtils::GetCommandLineArg( 2 );
	if( !latencyString.empty() )
	{
		latency = stof( latencyString );
	}
	NetworkManagerServer::sInstance->SetSimulatedLatency( latency );
}


int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	string portString = StringUtils::GetCommandLineArg( 1 );
	uint16_t port = stoi( portString );

	return NetworkManagerServer::StaticInit( port );
}

namespace 
{
	//In here I create all the SpawnWalls functions. Could have used a template but went with the quick and dirty option.
	
	//Set everything yourself but location & rotation is chosen randomly.
	void SpawnWalls(int numOfWalls, Vector3 minLocation, Vector3 maxLocation)
	{
		GetRandomVector GRV;
		
		for (int i = 0; i < numOfWalls; i++)
		{

			GameObjectPtr spawn = GameObjectRegistry::sInstance->CreateGameObject('WALL');
			spawn->SetRotation((float)rand());
			Vector3 wallLoc = GRV.GetRandVector(minLocation, maxLocation);
			spawn->SetLocation(wallLoc);
		}
	}
	//Set everything yourself including exact location
	void SpawnWalls(int numOfWalls, Vector3 location, float colRadius, float scale) 
	{
		for (int i = 0; i < numOfWalls; i++)
		{

			GameObjectPtr spawn = GameObjectRegistry::sInstance->CreateGameObject('WALL');
			spawn->SetScale(scale);
			spawn->SetCollisionRadius(colRadius);
			Vector3 wallLoc = Vector3(location.mX + (i + 1), location.mY + (i + 2), location.mZ);
			spawn->SetLocation(wallLoc);
		}
		
	}
	//Set just the number of walls and the default location boundaries will be set.
	void SpawnWalls(int numOfWalls) {
		GetRandomVector GRV;
		Vector3 defMinLoc(-5.0f, -3.0f, 0.0f);
		Vector3 defMaxLoc(5.0f, 3.0f, 0.0f);
		
		for (int i = 0; i < numOfWalls; i++)
		{

			GameObjectPtr spawn = GameObjectRegistry::sInstance->CreateGameObject('WALL');
			Vector3 wallLoc = GRV.GetRandVector(defMinLoc, defMaxLoc);
			spawn->SetLocation(wallLoc);
			spawn->SetScale(20.0f);
			spawn->SetCollisionRadius(1.0f);
			spawn->SetRotation((float)rand());
		}
	}

}

void Server::SetupWorld()
{
	//First try everything with random locations
	SpawnWalls(4, Vector3(-6.0f,1.0f,0.0f), Vector3(6.0f,-1.0f,0.0f));
	//Second try everything with predictable locations
	//SpawnWalls(5, Vector3(-5.0f, 0.0f, 0.0f), 0.5f, 10.0f);
	//Third try spawning just a number of walls using just the default locations
	//SpawnWalls(3);
	
}

void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnPlayers();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();
}

void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{

	int playerId = inClientProxy->GetPlayerId();

	//ScoreBoardManager::sInstance->AddEntry( playerId, inClientProxy->GetName() );
	inClientProxy->player = SpawnPlayer( playerId );
}

void Server::HandleOtherPlayerRespawnUponDeath(ClientProxyPtr inClientProxy) {
	int playerId = inClientProxy->GetPlayerId();

	if (playerId == 1) {
		//respawn player 2
		inClientProxy->player = SpawnPlayer(2);
	}
	else {
		//respawn player 1
		inClientProxy->player = SpawnPlayer(1);
	}
}

GameObject* Server::SpawnPlayer( int inPlayerId )
{
	PlayerPtr player = std::static_pointer_cast< Player >( GameObjectRegistry::sInstance->CreateGameObject( 'PLYR' ) );
	//player->SetColor( Colors::Red );//ScoreBoardManager::sInstance->GetEntry( inPlayerId )->GetColor() );
	player->SetPlayerId( inPlayerId );
	
	//player->SetLocation( Vector3( 1.f - static_cast< float >( inPlayerId ), 0.f, 0.f ) );

	//--------------------SPAWN LOCATION --------------------------- 
	//Spawn the first player in the bottom right of the arena 
	//Spawn the second player in the top left of the arena
	//Any further players are spawned in the centre
	if (inPlayerId == 1) {
		player->SetLocation(Vector3(5.5f, 2.7f, 0.0f));
	}
	else if (inPlayerId == 2) {
		player->SetLocation(Vector3(-6.5f, -4.0f, 0.0f));
		player->SetRotation(165.0f);
	}
	else {
		player->SetLocation(Vector3(0.0f, 0.0f, 0.0f));
	}
	int tempID = player->GetPlayerId();//get current playerID
	if (tempID == 1) {
		player->SetColor(Colors::White);//if first player on the server use this colour
	}
	if (tempID == 2) {
		player->SetColor(Colors::LightYellow); //if second use this colour
	}
	else {
		player->SetColor(Colors::LightBlue); //anyone else use this colour
	}

	return player.get();
}

void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's player
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();

	//ScoreBoardManager::sInstance->RemoveEntry( playerId );
	PlayerPtr player = GetPlayer( playerId );
	if( player )
	{
		player->SetDoesWantToDie( true );
	}
}

PlayerPtr Server::GetPlayer( int inPlayerId )
{
	//run through the objects till we find the Player...
	//it would be nice if we kept a pointer to the Player on the clientproxy
	//but then we'd have to clean it up when the Player died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];

		/* Original code did this in a weird way, used a method in the base (GameObject) which
		returned a player object if a game object is the player and null otherwise */

		uint32_t type = go->GetClassId();

		//Player* player = dynamic_cast<Player*>(*go);
		PlayerPtr player = nullptr;
		if(type == 'PLYR')
		{
			player = std::static_pointer_cast< Player >(go);
		}

		if(player && player->GetPlayerId() == inPlayerId )
		{
			return player;
		}
	}

	return nullptr;

}

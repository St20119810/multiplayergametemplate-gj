#include "NetworkManagerServer.h"
#include "Bullet.h"

/** 
BulletServer handles all the input requests from a client regarding their Bullet needs.

When a Player wants to shoot a bullet they send a request here, this request handles the necessary location and rotation for that given bullet and sends that information back to the Player for visual representation on their machine & any other connected hosts.

*/

class BulletServer : public Bullet 
{
public:
	///Registering the BulletServer with the NetworkManagerServer.
	static GameObjectPtr StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new BulletServer()); }
	///When the bullets need to be destroyed they are handled here.
	virtual void HandleDying() override;
	///Updating the bullet happens here/
	virtual void Update() override;
	///What happens when the bullet collides with a player.
	bool HandleCollisionWithPlayer(Player* player) override;

	//bool HandleCollisionWithWall(Wall* wall);

private:
	///Since the last update where were we in 3D space?
	Vector3 oldLocation;
	///Since the last update where were we looking?
	float oldRotation;
	///How long has this bullet been alive?
	float timeSinceCreation = 0.0f;
	///The bullet can live for a maximum amount of time for 10 seconds, in case the collision walls fail it won't go off into infinity.
	const float maxAllowedLifetime = 10.0f;

};
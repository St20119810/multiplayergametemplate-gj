#ifndef SERVER_H
#define SERVER_H

#include "Engine.h"
#include "ClientProxy.h"
#include "Player.h"
#include "NetworkManagerServer.h"
/** 
Server extends Engine.

The gameloop must be ran on the server, hence the extension of Engine. 
The server handles all of the Clients and what they do each frame.
*/
class Server : public Engine
{
public:

	static bool StaticInit();
	///This sets up the ordering in which requests and events are handled then executed.
	virtual void DoFrame() override;
	///This sets up the world or arena in which the game will be played.
	virtual int Run();

	///When a new Client joins it must be processed and added to the list of connected Clients.
	void HandleNewClient( ClientProxyPtr inClientProxy );
	///When a Client disconnects this is handled here, when they are lost the server removes their Id and takes them off the scoreboard.
	void HandleLostClient( ClientProxyPtr inClientProxy );

	void HandleOtherPlayerRespawnUponDeath(ClientProxyPtr inClientProxy);

	///This is set up crudely to dish out events to the correct Client. So first we must identify who is who using this function.
	PlayerPtr	GetPlayer( int inPlayerId );
	///When a Client joins they must be spawned in, this is handled here.
	GameObject*	SpawnPlayer( int inPlayerId );


private:
	Server();

	bool	InitNetworkManager();
	void	SetupWorld();

};

#endif // SERVER_H

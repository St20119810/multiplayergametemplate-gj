#ifndef PLAYER_SERVER_H
#define PLAYER_SERVER_H

#include "Player.h"
#include "NetworkManagerServer.h"
/** 
This class is used to handle all the server updating for a player.
*/
///The player in the game can either be controller by a real player or an AI, set that here.
enum EPlayerControlType
{
	ESCT_Human,
	ESCT_AI
};

class PlayerServer : public Player
{
public:
	///Ensuring the server has a version of the player from that host when they connect.
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new PlayerServer() ); }
	///When they die the server should handle what happens then send that data out to the client.
	virtual void HandleDying() override;
	///This update is actually used, this centralises all the updating of each client to the server. That control should help us stay synced.
	virtual void Update() override;
	///Implementing the control types set up in the header.
	void SetPlayerControlType( EPlayerControlType inPlayerControlType ) { mPlayerControlType = inPlayerControlType; }
	///When that player is hit the server handles who's playerId was hit then streams out that information to the connected clients. 
	void TakeDamage( int inDamagingPlayerId );

protected:
	///The constructor, the player type and their speed of shots are set up here.
	PlayerServer();

private:
	///When the request from a client to shoot comes in, this handles what that should do.
	void HandleShooting();

	EPlayerControlType	mPlayerControlType;


	float		mTimeOfNextShot;
	float		mTimeBetweenShots;

};

#endif // PLAYER_SERVER_H

#include "BulletServer.h"
#include "Maths.h"
#include "Player.h"

void BulletServer::HandleDying()
{
	Bullet::HandleDying();
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

void BulletServer::Update()
{
	Bullet::Update();
	
	float deltaTime = Timing::sInstance.GetDeltaTime();

	timeSinceCreation += deltaTime;
	if (timeSinceCreation > maxAllowedLifetime) {
		SetDoesWantToDie(true);
		return;
	}

	SetLocation(GetLocation() + GetForwardVector() * bulletSpeed * deltaTime);

	if (!Maths::Is2DVectorEqual(oldLocation, GetLocation()) || oldRotation != GetRotation()) 
	{
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), EBRS_Pose);
	}

	oldLocation = GetLocation();
	oldRotation = GetRotation();
}

bool BulletServer::HandleCollisionWithPlayer(Player* player)
{
	if (DoesWantToDie()) return false;
	if (player == theShooter) return false;

	player->TakeDamage(theShooter->GetPlayerId());

	SetDoesWantToDie(true);
	return true;
}

//bool BulletServer::HandleCollisionWithWall(Wall* wall)
//{
//	if (DoesWantToDie()) return false;
//
//	SetDoesWantToDie(true);
//	return true;
//}

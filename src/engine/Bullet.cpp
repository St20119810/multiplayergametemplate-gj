#include "Bullet.h"
#include "Player.h"
#include "OutputMemoryBitStream.h"

const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

Bullet::Bullet()
{
	SetScale(0.4f);
	SetCollisionRadius(0.1f);
}

void Bullet::Update()
{
	ProcessCollisionsWithScreenWalls();

	float sourceRadius = GetCollisionRadius();
	Vector3 sourceLocation = GetLocation();

	//now let's iterate through the world and see what we hit...
	//note: since there's a small number of objects in our game, this is fine.
	//but in a real game, brute-force checking collisions against every other object is not efficient.
	//it would be preferable to use a quad tree or some other structure to minimize the
	//number of collisions that need to be tested.
	for (auto goIt = World::sInstance->GetGameObjects().begin(), end = World::sInstance->GetGameObjects().end(); goIt != end; ++goIt)
	{
		GameObject* target = goIt->get();
		if (target != this && !target->DoesWantToDie())
		{
			//simple collision test for spheres- are the radii summed less than the distance?
			Vector3 targetLocation = target->GetLocation();
			float targetRadius = target->GetCollisionRadius();

			Vector3 delta = targetLocation - sourceLocation;
			float distSq = delta.LengthSq2D();
			float collisionDist = (sourceRadius + targetRadius) * 0.8f;
			if (distSq < (collisionDist * collisionDist))
			{
				SetDoesWantToDie(true);
			}
		}
	}
}

void Bullet::Init(Player* player) {
	theShooter = player;
}

void Bullet::ProcessCollisionsWithScreenWalls()
{
	Vector3 location = GetLocation();
	float x = location.mX;
	float y = location.mY;

	float radius = GetCollisionRadius();

	
	if ((y + radius) >= HALF_WORLD_HEIGHT)
	{
		location.mY = HALF_WORLD_HEIGHT - radius;
		SetDoesWantToDie(true);
	}
	else if (y <= (-HALF_WORLD_HEIGHT - radius))
	{
		
		location.mY = -HALF_WORLD_HEIGHT - radius;
		SetDoesWantToDie(true);
	}

	if ((x + radius) >= HALF_WORLD_WIDTH)
	{
		
		location.mX = HALF_WORLD_WIDTH - radius;
		SetDoesWantToDie(true);
	}
	else if (x <= (-HALF_WORLD_WIDTH - radius))
	{
		
		location.mX = -HALF_WORLD_WIDTH - radius;
		SetDoesWantToDie(true);
	}
}

uint32_t Bullet::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EBRS_Pose)
	{
		inOutputStream.Write((bool)true);

	

		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		inOutputStream.Write(GetRotation());

		writtenState |= EBRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}



#pragma once
#include <GameObject.h>

/*! The bullet class located in engine.

	The bullet class extends off of the \class GameObject class.
	This class is located in the engine and will be extended for use in the Server and Client.
 *
 */
class Bullet : public GameObject
{
public:
	CLASS_IDENTIFICATION('BULL', GameObject)



		enum EBulletReplicationState
	{
		EBRS_Pose = 1 << 0,
		/*EBRS_Color = 1 << 1,
		EBRS_PlayerId = 1 << 2,
		EBRS_Health = 1 << 3,*/

		EBRS_AllState = EBRS_Pose /*| EBRS_Color | EBRS_PlayerId | EBRS_Health*/
	};

	uint32_t GetAllStateMask()    const override { return EBRS_AllState; }
	///This is update function to be used by all extensions of the class. 
		virtual void Update() override;


	///When a bullet hits something the collision is handled here.
	void ProcessCollisions();
	///When the bullet hits a wall something should happen, it shouldn't just keep going forever.
	void ProcessCollisionsWithScreenWalls();

	//This is used for testing, to fine tune the bullet and ensure it behaves as expected.
	//const Vector3& GetVelocity() const { return mVelocity; }

	///To send over the data stream and ultimately to the server.
	uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;


	//This is also used for testing and ensuring correct behaviour.
	//float GetMaxLinearSpeed() { return mMaxLinearSpeed; }

	//float GetLastMoveTimestamp() { return mLastMoveTimestamp; }

	///This is the set up for whom shot the bullet.
	void Init(Player* player);

protected:
	///The constructor for the bullet, this will set the actual speed of the bullet when fired by the player. The actual speed of the bullet may change if powerups are implemented.
	Bullet();
	
	///By default the bullets speed will be this
	const float bulletSpeed = 1.0f;
	///To keep track of whom shot what this can be used. Mainly used for testing and debugging for when things go wrong. Could be extended to help keep track of multiple players if more than 2 player arenas become a thing.
	Player* theShooter = nullptr;
};


#include "Wall.h"

Wall::Wall() {
	SetScale(4.0f);
	SetCollisionRadius(0.9f);
}

//bool Wall::HandleCollisionWithPlayer(Player* inPlayer) {
//	(void)inPlayer;
//	
//	return true;
//}

uint32_t Wall::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const 
{
	uint32_t writtenState = 0;

	if (inDirtyState & EWRS_Pose) 
	{
		inOutputStream.Write((bool)true);
		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		inOutputStream.Write(GetRotation());

		writtenState |= EWRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool) false);
	}

	return writtenState;
}

void Wall::Read(InputMemoryBitStream& inInputStream) 
{
	bool stateBit;

	inInputStream.Read(stateBit);
	if (stateBit) 
	{
		Vector3 location;
		inInputStream.Read(location.mX);
		inInputStream.Read(location.mY);
		SetLocation(location);

		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);
	}
	//inInputStream.Read(stateBit);
	/*if (stateBit) 
	{
		Vector3 colour;
		inInputStream.Read(colour);
		SetColor(colour);
	}*/
}
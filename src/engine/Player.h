#ifndef PLAYER_H_
#define PLAYER_H_

#include "GameObject.h"
#include "World.h"

class InputState;

/** 
	This class is the base class for the PlayerClient and PlayerServer classes.
*/

class Player : public GameObject
{
public:
	CLASS_IDENTIFICATION( 'PLYR', GameObject )

	enum EPlayerReplicationState
	{
		EPRS_Pose = 1 << 0,
		EPRS_Color = 1 << 1,
		EPRS_PlayerId = 1 << 2,
		EPRS_Health = 1 << 3,
		EPRS_SCORE = 1 << 4,

		EPRS_AllState = EPRS_Pose | EPRS_Color | EPRS_PlayerId | EPRS_Health | EPRS_SCORE
	};

	///This is used to create a player in the game when a client joins
	static	GameObject*	StaticCreate()			{ return new Player(); }
	///Jenkins Note - the code in the book doesn't provide this until the client. This however limits testing.
	static	GameObjectPtr	StaticCreatePtr()			{ return GameObjectPtr(new Player()); }

	virtual uint32_t GetAllStateMask()	const override	{ return EPRS_AllState; }

	virtual void Update() override;

	virtual void TakeDamage(int inDamaginPlayerId) {}

	void ProcessInput( float inDeltaTime, const InputState& inInputState );
	void SimulateMovement( float inDeltaTime );
	///This is the overall update for Collisions
	void ProcessCollisions();
	///This happens inside of ProcessCollisions() it only checks when the player his a wall and respons with what to do in that event.
	void ProcessCollisionsWithScreenWalls();
	///PlayerID is used in determining which tank should be used and allows the players to identify a name in game.
	void		SetPlayerId( uint32_t inPlayerId )			{ mPlayerId = inPlayerId; }
	///Used to check what a players ID is.
	uint32_t	GetPlayerId()						const 	{ return mPlayerId; }
	///When a player gives input to move this function sets the players velocity.
	void			SetVelocity( const Vector3& inVelocity )	{ mVelocity = inVelocity; }
	const Vector3&	GetVelocity()						const	{ return mVelocity; }


//	virtual void	Read( InputMemoryBitStream& inInputStream ) override;

	uint32_t Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const override;
	
	//For testing
	///Testing the max speed of the player.
	float GetMaxLinearSpeed() { return mMaxLinearSpeed; }
	///Testing the max rotation of the player.
	float GetMaxRotationSpeed() { return mMaxRotationSpeed; }
	///Used to test the bounciness of walls
	float GetWallRestitution() { return mWallRestitution; }
	///If players collide, what should happen? This tests that.
	float GetNPCRestitution() { return mNPCRestitution; }
	///Checking whether the player is where they are supposed to be
	float GetLastMoveTimestamp() { return mLastMoveTimestamp; }
	///When an input is given what is the direction?
	float GetThrustDir() { return mThrustDir; }
	///The health of the player
	int GetHealth() { return mHealth; }
	///Checking states, are they supposed to be firing?
	bool IsShooting() { return mIsShooting; }

	bool operator==(Player &other);
protected:
	///The constructor for the Player Object.
	Player();

private:

	///Used for testing.
	void	AdjustVelocityByThrust( float inDeltaTime );
	///Used for testing.
	Vector3				mVelocity;

	///Used for testing.
	float				mMaxLinearSpeed;
	///Used for testing.
	float				mMaxRotationSpeed;

	//bounce fraction when hitting various things
	///Used for testing.
	float				mWallRestitution;
	///Used for testing.
	float				mNPCRestitution;

	///Used for identification.
	uint32_t			mPlayerId;

protected:

	//move down here for padding reasons...
	///Used for testing.
	float				mLastMoveTimestamp;
	///Used for testing.
	float				mThrustDir;
	///Used for testing.
	int					mHealth;
	///Used for testing.
	bool				mIsShooting;


};

typedef shared_ptr< Player >	PlayerPtr;

#endif // PLAYER_H_

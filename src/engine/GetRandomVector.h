#include "Maths.h"

/** 
Taken from the Multiplayer Programming book by Glazer & Madhav.

The GetRandomVector and GetRandomFloat functions were written by Glazer and Madhav, I implemented them to have random locations generated for the Walls.
*/
class GetRandomVector 
{
public:

	const float pi = 3.1415926535f;

	float GetRandomFloat();

	Vector3 GetRandVector(const Vector3& inMin, const Vector3& inMax);
};
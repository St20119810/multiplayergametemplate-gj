#include "GameObject.h"
#include "OutputMemoryBitStream.h"
#include "InputMemoryBitStream.h"

/** 
The Wall is intended to be a static object that the players can use for cover.


*/

class Wall : public GameObject
{
public:
	///The Class identification should allow for the setting up of references to which object is which.
	CLASS_IDENTIFICATION('WALL', GameObject)

	enum EWallReplicationState 
	{
		///I only get the pose and send that data.
		EWRS_Pose = 1 << 0,

		EWRS_AllState = EWRS_Pose
	};
	///Necessary to send the correct amount of data across
	uint32_t GetAllStateMask()    const override { return EWRS_AllState; }
	///The static creation of the wall.
	static GameObject* StaticCreate() { return new Wall(); }

	///For when a player has collided with a wall this can be written into the local client.
	virtual uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	///When a player has collided with a wall this has to be sent out over the data stream to be updated to each client.
	virtual void Read(InputMemoryBitStream& inInputStream) override;


	//If I wanted to implement a way for the walls to be destroyed over a certain number of hits I could start here.
	/*virtual bool HandleCollisionWithPlayer(Player* inPlayer) override;*/

	//void Init(Player* player);

protected:
	///In this constructor the scale and collision radius is set up.
	Wall();
};
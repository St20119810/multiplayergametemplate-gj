/*****************************************************************//**
 * \mainpage Pub Tank Game
 * \author Tom Hudson
 * \date   April 2020
 *______________________________________________
 * \section intro_sec Introduction
 * This networking codebase builds from Glazer and Madhav, it was then extended by Jenkins.
 * Now I am building a tank game on top of that.
 *
 * \subsection Brief
 * A small games company is looking to develop a game for a major client, which requires the development of an SDL2 based tank battle game.  This will run on low end hardware running basic embedded versions of the Windows/Linux operating system.  The company currently makes gambling games (fruit machines, quiz machines etc.) and has extensive experience of stand alone games.  This will be their first attempt to produce a client server game for the pub market.
The companies managing director has asked for a clone of the classic Ultra Tank arcade game (originally produced by Kee Games).  More information and videos of the original game can be found here (https://www.arcade-museum.com/game_detail.php?game_id=10261).  This was a childhood favourite of the MD and one he believes will be simple enough for most patrons to play.  Updating the sprites and including some more sophisticated (relevant) game mechanics will enhance the appeal while networking will allow for matches between pubs while keeping local players physically separated. Several other additional features, such as one or more NPC tanks along with short CPU vs CPU battles which take place while the game is idle have also been suggested.
This will be the firstgame of its kind produced by the company and there is some caution amongst management and the development team.  A prototype is being developed focusing on the networking component, the performance of which will be key to the game and its success.
You are required to work on a prototype game which will provide an arena for two or more tanks to join hosted on a remote server.  Additional features such as firing weapons, audio, pick-ups can be implemented if time allows.  Provide written documentation for the software produced in a suitable format and provides a report on the unit/play testing undertaken.

   \subsection Repository 
    Provided is the link to the repository that contains all the source code documented here. <a href="https://bitbucket.org/St20119810/multiplayergametemplate-gj/src/master/">BitBucket Repository</a>

   \subsection Dropbox
    Provided here are the videos highlighting the game being played and tested. [LINK]

	\subsection Installing
	Throughout development I came across some weird cmake bugs and spent hours trying to fix them. I could not get my cmake files to properly install the x64 versions of the SDL_image dll's. This means if when you're building the game you run into a similar error, just manually grab the correct dll's and pop them in the build/dist/bin folder.
______________________________________________________________________
 * \subsection d My Focus
 * I have implemented a 1v1 tank shooter allowing two players physically separate to fight against one another in an arena setting.
 *
 * \subsection c Game Mechanics
 * It was key to implement an easy to pick up and play shooter that was simple and fun.
 * \subsubsection v Core Mechanic
   The core mechanic or draw of the game is the multiplayer. Even with no shooting mechanics it is simply fun to mess around in an arena with a mate. The multiplayer is achieved through the use of Client/Server networking. Specifically this codebase implements a UDP connection.

   \subsubsection b Primary Mechanics
 * \b Shooting - The player can fire using the r key, this is used to kill and defeat the other tank.
 *
 * \b Moving - The player can move around the arena in 8 directions, this is used to dodge incoming fire and adjust your own shot.
 *

 ______________________________________


  \subsection Testing

  \subsubsection g Pushing the Server
  I wanted to test out how much load the server could take. This would give an idea of how much head room the server has. 
  
  \subsubsection Players
  The first limit test I did was testing the amount of players that can be connected before the server crashes. The implementation only intends to handle 2 clients connected but this can be extended for more game modes like tag team for example.
  
  The video concluded after 5 clients connected, after that the video recording software failed. However, on my PC I managed to connect 12 clients before the game became unplayable and the server crashed.
  

  \subsubsection Bullets
  The second limit test revolves around bullets. With the max number of clients documented it is logical to next figure out how many bullets can exist at any one time.
  The bullets are supposed to be destroyed upon impact with anything so this should already help with the optimisation of the code. 
  For this test, there are 2 variables. Variable number 1 is the rate of fire for all players. Variable number 2 is the number of clients firing at any one time.
  I am expecting the server to completely fall over with high player counts and a high rate of fire but as the player count and rate of fire decreases, the stability of the server increases.

  Test #1 Time between Shots(RoF) is 0 & Player count is 10 - Results: With this setup the game slowed down significantly when all 10 players were spamming bullets. Not recommended as a playable game state. 

  Test #2 Time between Shots(RoF) is 0 & Player count is 5 - Results: With this setup the game ran smoothly. This is certainly viable as a playable game state.

  Test #3 Time between Shots(RoF) is 0 & Player count is 2 - Results: The game had no hiccups and ran smoothly. This is another viable game state.

  Test #4 Time between Shots(RoF) is 2 & Player count is 10 - Results: The performance was a lot better here. This could be a playable state depending on the connection quality of clients.

  Test #5 Time between Shots(RoF) is 2 & Player count is 5 - Results: Game ran smoothly.

  Test #6 Time between Shots(RoF) is 2 & Player count is 2 - Results: Probably optimal game state, bullets do not lag the game at all.

  Test #7 Time between Shots(RoF) is 4 & Player count is 10 - Results: There is no performance issues here but the game isn't really fun.


  \subsubsection Lag
  Now that it is established in peak scenarios packet scenarios what the stability of the server is like, I am now going to test the game in its most realistic state (1v1) with varying levels of packet loss.
  This should help identify the playabiliity of the game over poor connections.

  First test the chance to drop a packet is set to 0 simulating a perfect state. Results: Obviously a very smooth playtest.

  Second test the chance to drop a packet is set to 0.1. Results: Connection is jittery. This makes for unstable gameplay.

  Third test the chance to drop a packet is set to 0.5. Results: Connection is completely unstable and therefore gameplay is unplayable.

  \subsection Feedback
  I issued a questionnaire for some playtesters to answer throughout their playtine. The questionnaires were taken informally and are for interal feedback and improvements of the game only.
  The questions asked in this questionnaire are:

  1)Did you find the game enjoyable?

  2)Did you know the wall change position each time you launch the game? Did this improve replayability or not in your opinion?

  3)What would you add to make this game more enjoyable in a pub setting?

 \b Results:

  Three playtesters participated in the feedback questionnaire. 

  Person #1 

  1) I did enjoy the game, yes.

  2) I did realise the walls were changing. This wasn't subtle and sometime the walls are weirdly spaced, bullets can fly through them when it looks like they shouldn't.
  
  3)I would add more accurate collision detection and some cool sound effects.


  Person #2

  1)I did not enjoy the game at all.

  2)I did see that but don't think it added to replayability.

  3)This game doesn't work in the pubs because keyboard controls are not fun. Maybe if the controls worked on a traditional controller that would help the average joe understand better what they can do.


  Person #3

  1)I did not enjoy the game.

  2)Yes, I noticed it. The art kinda sucked so I don't think it added much to the game.

  3)For it to be fun in the pub I would want to play with more than just one person in another pub, maybe having team battles or something?



***********************************************************************/

#include "GetRandomVector.h"
#include <random>

float GetRandomVector::GetRandomFloat() {
	static std::random_device rd;
	static std::mt19937 gen(rd());
	static std::uniform_real_distribution< float > dis(0.0f, 1.0f);
	return dis(gen);
}

Vector3 GetRandomVector::GetRandVector(const Vector3& inMin, const Vector3& inMax) {
	Vector3 radius = Vector3(GetRandomFloat(), GetRandomFloat(), GetRandomFloat());
	return inMin + (inMax - inMin) * radius;
}

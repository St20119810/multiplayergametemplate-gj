#include "WallClient.h"
#include "TextureManager.h"

WallClient::WallClient() 
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("Wall"));
}


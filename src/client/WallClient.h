#include "Wall.h"
#include "SpriteComponent.h"
/** 
The client side of the wall object.

This class isn't fancy at all, it is used to set up the sprite component of all the walls.
*/
class WallClient : public Wall 
{
public:
	///Ensuring a local wall is created.
	static GameObjectPtr StaticCreate() { return GameObjectPtr(new WallClient()); }
protected:
	///Setting up the WallClients SpriteComponent.
	WallClient();
private:
	///Need to have a local call to the Sprite Component.
	SpriteComponentPtr mSpriteComponent;
};
#include "PlayerClient.h"

#include "TextureManager.h"
#include "GameObjectRegistry.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"

#include "StringUtils.h"
#include "TextItem.h"
#include "RenderManager.h"


PlayerClient::PlayerClient() :
	mTimeLocationBecameOutOfSync( 0.f ),
	mTimeVelocityBecameOutOfSync( 0.f )
{
	scoreHeaderText = new TextItem();
	scoreHeaderText->setText("Score: ");
	RenderManager::sInstance->AddComponent(scoreHeaderText);

	scoreText = new TextItem();
	scoreText->setText(" s");
	RenderManager::sInstance->AddComponent(scoreText);

	HealthHeaderText = new TextItem();
	HealthHeaderText->setText("Health: ");
	RenderManager::sInstance->AddComponent(HealthHeaderText);

	healthText = new TextItem();
	healthText->setText("");
	RenderManager::sInstance->AddComponent(healthText);

	mSpriteComponent.reset( new SpriteComponent( this ) );
	mSpriteComponent->SetTexture( TextureManager::sInstance->GetTexture( "player" ) );
}

void PlayerClient::HandleDying()
{
	Player::HandleDying();
	RenderManager::sInstance->RemoveComponent(scoreHeaderText);
	delete scoreHeaderText;

	RenderManager::sInstance->RemoveComponent(scoreText);
	delete scoreText;

	RenderManager::sInstance->RemoveComponent(HealthHeaderText);
	delete HealthHeaderText;

	RenderManager::sInstance->RemoveComponent(healthText);
	delete healthText;

}


void PlayerClient::Update()
{
	//for now, we don't simulate any movement on the client side
	//we only move when the server tells us to move
	if (GetPlayerId() == 1) {
		//set UI for player 1
		scoreHeaderText->box.x = 1600;
		scoreText->box.x = 1700;
		scoreText->box.w = 50;
		scoreText->box.h = 50;
		HealthHeaderText->box.x = 1600;
		HealthHeaderText->box.y = 100;
		healthText->box.x = 1700;
		healthText->box.y = 100;
		healthText->box.w = 50;
		healthText->box.h = 50;
	}
	else if (GetPlayerId() == 2) {
		//set UI for player 2
		scoreHeaderText->box.y = 700;
		scoreText->box.x = 100;
		scoreText->box.y = 700;
		scoreText->box.w = 50;
		scoreText->box.h = 50;
		HealthHeaderText->box.y = 750;
		healthText->box.x = 100;
		healthText->box.y = 750;
		healthText->box.w = 50;
		healthText->box.h = 50;
	}
	else {
		//don't set UI.
	}
	
	}

void PlayerClient::Read( InputMemoryBitStream& inInputStream )
{
	bool stateBit;

	uint32_t readState = 0;

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		uint32_t playerId;
		inInputStream.Read( playerId );
		SetPlayerId( playerId );

		//switch sets up what sprite a player should display as 
		switch (playerId) {
		case 1:
			mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("player"));
			break;
		case 2:
			mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("tank2"));
			break;
		default:
			mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("tank3"));

		}

		readState |= EPRS_PlayerId;
	}

	float oldRotation = GetRotation();
	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();

	float replicatedRotation;
	Vector3 replicatedLocation;
	Vector3 replicatedVelocity;

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		inInputStream.Read( replicatedVelocity.mX );
		inInputStream.Read( replicatedVelocity.mY );

		SetVelocity( replicatedVelocity );

		inInputStream.Read( replicatedLocation.mX );
		inInputStream.Read( replicatedLocation.mY );

		SetLocation( replicatedLocation );

		inInputStream.Read( replicatedRotation );
		SetRotation( replicatedRotation );

		readState |= EPRS_Pose;
	}

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		inInputStream.Read( stateBit );
		mThrustDir = stateBit ? 1.f : -1.f;
	}
	else
	{
		mThrustDir = 0.f;
	}

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		Vector3 color;
		inInputStream.Read( color );
		SetColor( color );
		readState |= EPRS_Color;
	}

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		mHealth = 0;
		inInputStream.Read( mHealth, 4 );
		readState |= EPRS_Health;
		healthText->setText(std::to_string(mHealth));
	}

}

#pragma once
#include "SDL_ttf.h"
#include "SDL.h"
#include <string>


/** The TextItem sets up everything necessary for the UI to show up on screen.

If the game were extended this class could be used to for all text UI but aan image UI class would need to implemented to show images in the HUD.
*/
using namespace std;
class TextItem {
public:
	///The constructor sorts out the sizings and colour of the text.
	TextItem();
	///To draw the TextItem this function is used.
	void Draw();
	///When wanting to set what the text reads use this function.
	void setText(string);
	///When setting the text dimensions manually, use this function.
	SDL_Rect box;

private:

	
	///What font are you using? Must use a ttf file.
	TTF_Font* font;
	///Set the only colour available to white because its easiest to see.
	SDL_Color colourWhite;
	///The surface the texture will be put on.
	SDL_Surface* surface;
	///The texture of the TextItem.
	SDL_Texture* texture;
};

//I take care of rendering things!

#ifndef RENDER_MANAGER_H
#define RENDER_MANAGER_H

#include <memory>
#include <vector>
using std::vector;

#ifndef _WIN32
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif
#include "TextItem.h"

class SpriteComponent;
class Background;

class RenderManager
{
public:

	static void StaticInit();
	static std::unique_ptr< RenderManager >	sInstance;

	void Render();
	void RenderComponents();

	//vert inefficient method of tracking scene graph...
	void AddComponent( SpriteComponent* inComponent );
	void RemoveComponent( SpriteComponent* inComponent );
	int	 GetComponentIndex( SpriteComponent* inComponent ) const;

	void AddComponent(TextItem* inTextItem);
	void RemoveComponent(TextItem* inTextItem);
	int	 GetComponentIndex(TextItem* inTextItem) const;

	//this is not a good way of doing this. 
	void AddBackground(Background *inBackground) {background = inBackground;};

private:

	RenderManager();

	//this can't be only place that holds on to component- it has to live inside a GameObject in the world
	vector< SpriteComponent* >		mComponents;
	vector< TextItem* >		textItemComponents;

	SDL_Rect						mViewTransform;

	// Background 
	Background *background;

};

#endif // RENDER_MANAGER_H

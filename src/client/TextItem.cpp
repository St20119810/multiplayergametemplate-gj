#include "TextItem.h"
#include "GraphicsDriver.h"

TextItem::TextItem()
{
	font = TTF_OpenFont("../assets/Carlito-Regular.ttf", 24);
	colourWhite = { 255,255,255 };
	box = { 0,0, 100,50 };
	setText("Hello World");
}
void TextItem::setText(string inText) {
	surface = TTF_RenderText_Solid(font, inText.c_str(), colourWhite);
	texture = SDL_CreateTextureFromSurface(GraphicsDriver::sInstance->GetRenderer(), surface);
}
void TextItem::Draw() {
	SDL_RenderCopyEx(GraphicsDriver::sInstance->GetRenderer(), texture, NULL, &box, 0, NULL, SDL_FLIP_NONE);
}

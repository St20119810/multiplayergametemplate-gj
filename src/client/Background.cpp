#include "Background.h"
#include "GraphicsDriver.h"
#include "RenderManager.h"
#include "StringUtils.h"

Background::Background() 
{

}

void Background::init()
{
	//and add yourself to the rendermanager...
	RenderManager::sInstance->AddComponent( this );
	
	TexturePtr tex = TextureManager::sInstance->GetTexture("back");
	this->SetTexture(tex);
}

Background::~Background()
{
	//don't render me, I'm dead!
	RenderManager::sInstance->RemoveComponent( this );
}


void Background::Draw( const SDL_Rect& inViewTransform )
{
	if( mTexture )
	{
		//Looked at scaling the image to the size of the viewport, but didn't get it working. 

		// Compute the destination rectangle
		//Vector3 objLocation(0.0f,0.0f,0.0f);

		//SDL_Rect dstRect;
		//dstRect.w = static_cast< int >( mTexture->GetWidth() * objScale );
		//dstRect.h = static_cast< int >( mTexture->GetHeight() * objScale );

		// fudge the image scale so its the right width or height
		//float objScaleX =  GraphicsDriver::sInstance->GetLogicalViewport().w / mTexture->GetWidth();
		//float objScaleY = GraphicsDriver::sInstance->GetLogicalViewport().h / mTexture->GetHeight();

		//dstRect.w = static_cast< int >( mTexture->GetWidth() * objScaleX );
		//dstRect.h = static_cast< int >( mTexture->GetHeight() * objScaleY );

		// Factor screen size in when drawing. 
		//dstRect.x = static_cast<int>( objLocation.mX * inViewTransform.w + inViewTransform.x - dstRect.w / 2 );
		//dstRect.y = static_cast<int>( objLocation.mY * inViewTransform.h + inViewTransform.y - dstRect.h / 2 );
		
		// Blit the texture
		SDL_RenderCopyEx( GraphicsDriver::sInstance->GetRenderer(), mTexture->GetData(), nullptr,
			nullptr, 0.0f , nullptr, SDL_FLIP_NONE );

			//LOG("DEBUG: %s", "Rendering, do you see me!");
	}
	else
	{
		LOG("Texture not Loaded, GameObject ID is: %s\n","background");
	}
}


#include "BulletClient.h"

BulletClient::BulletClient()
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("bullet"));
}

void BulletClient::Update()
{
	//done on server
}

void BulletClient::HandleDying()
{
	Bullet::HandleDying();
}

void BulletClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	uint32_t readState = 0;
	float oldRotation = GetRotation();
	Vector3 oldLocation = GetLocation();
	//Vector3 oldVelocity = GetVelocity();

	float replicatedRotation;
	Vector3 replicatedLocation;
	//Vector3 replicatedVelocity;

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		inInputStream.Read(replicatedLocation.mX);
		inInputStream.Read(replicatedLocation.mY);

		SetLocation(replicatedLocation);

		inInputStream.Read(replicatedRotation);
		SetRotation(replicatedRotation);

		readState |= EBRS_Pose;
	}
}

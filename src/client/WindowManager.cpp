#include "WindowManager.h"
#include "Vector3.h"

std::unique_ptr< WindowManager >	WindowManager::sInstance;

bool WindowManager::StaticInit()
{
	Vector3 viewport(1024, 768, 0);
	SDL_Window* wnd = SDL_CreateWindow( "Tank shoot-em-up", viewport.mX / 4, viewport.mY / 4, viewport.mX, viewport.mY, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE );

	if (wnd == nullptr)
	{
		SDL_LogError( SDL_LOG_CATEGORY_ERROR, "Failed to create window." );
		return false;
	}

 	sInstance.reset( new WindowManager( wnd ) );

	return true;
}


WindowManager::WindowManager( SDL_Window* inMainWindow )
{
	mMainWindow = inMainWindow;

}

WindowManager::~WindowManager()
{
	SDL_DestroyWindow( mMainWindow );
}

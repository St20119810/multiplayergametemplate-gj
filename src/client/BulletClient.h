#include "Bullet.h"
#include "InputMemoryBitStream.h"
#include "SpriteComponent.h"
#include "TextureManager.h"

/** 
BulletClient handles the information sent from the BulletServer. 
*/

class BulletClient : public Bullet
{
public:
	///In the constructor the scale and collision radius is set for every bullet.
	BulletClient();

	///Ensuring a bullet is created.
	static GameObjectPtr StaticCreate() 
	{
		BulletClient* bull = new BulletClient();
		return GameObjectPtr(bull);
	}
	///This does nothing here as all updating is handled in BulletServer.
	virtual void Update();
	///When a BulletClient wants to die, the Bullet HandleDying is called.
	virtual void HandleDying() override;

	///Read any data in from the stream and update the local client.
	void Read(InputMemoryBitStream& inInputStream);
private:
	///To have access to the sprite component of the bullet.
	SpriteComponentPtr mSpriteComponent;
};
#ifndef PLAYER_CLIENT_H
#define PLAYER_CLIENT_H

#include "Player.h"
#include "SpriteComponent.h"

class TextItem;

/**
	Player Client is a class controlling the players client side actions.

	This class reads to the output stream for all local client actions

*/

class PlayerClient : public Player
{
public:
	///Create the player on the client
	static	GameObjectPtr	StaticCreate()		{ return GameObjectPtr( new PlayerClient() ); }

	///If needed, but all simulation is currently done with the server
	virtual void Update();
	///What should happen when a player needs to die
	virtual void	HandleDying() override;

	///When the server sends us a packet we need to read it back, this handles it
	virtual void	Read( InputMemoryBitStream& inInputStream ) override;


	TextItem* scoreHeaderText = nullptr;
	TextItem* scoreText = nullptr;
	TextItem* HealthHeaderText = nullptr;
	TextItem* healthText = nullptr;

protected:
	PlayerClient();


private:

	///Used for testing to ensure synchronisation 
	float				mTimeLocationBecameOutOfSync;
	///Used for testing to ensure synchronisation 
	float				mTimeVelocityBecameOutOfSync;

	///Used when setting the sprite of the player from Texture Manager
	SpriteComponentPtr	mSpriteComponent;
};
typedef shared_ptr< PlayerClient >	PlayerClientPtr;
#endif //PLAYER_CLIENT_H


#ifndef _WIN32
#include <SDL2/SDL.h>
#else
#include <SDL.h>
#endif

#include <iostream>
#include "SpriteComponent.h"
#include "TextureManager.h"
#include "GameObject.h"
//#include "StringUtils.h"


/**
This class provides the functionality to change the background image
*/
class Background : public SpriteComponent
{
public:
	Background();
	~Background();
	void Draw( const SDL_Rect& inViewTransform );
	void	SetTexture( TexturePtr inTexture )			{ mTexture = inTexture; }
	void init();

private:
	TexturePtr	mTexture;
	


};

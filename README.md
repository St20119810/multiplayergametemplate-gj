# Cross Platform Replication Wrapper

This is a small cross platform (and hopefully fairly general) replication wrapper (plus ~~some~~ almost no testing), most of the core code is based on [Multiplayer Game Programming: Architecting Networked Games](https://www.oreilly.com/library/view/multiplayer-game-programming/9780134034355/) - by Josh Glazer.  This project is looking to extract out the core library allowing students in the third year Multi-Player Game Development module to use it in their own games having read the relevent book chapters.

## Plan ##

Need to write this somewhere so I don't forget short term.

Plan to add to this:
* Types of packet (means of identifying transmitted informaiton)
* Replication Manager
  * Naieve World Replication
  * Delta based
* RPCs
* MVE for streams of objects

Remove:
* Unrelated test stuff concerning serialsiation etc.
